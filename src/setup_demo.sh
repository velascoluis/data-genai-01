#!/bin/sh
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#........................................................................
# Purpose: Setup demo
#........................................................................

if [ ! "${CLOUD_SHELL}" = true ]; then
    echo "This script needs to run on Google Cloud Shell. Exiting ..."
    exit ${ERROR_EXIT}
fi

GCLOUD_BIN=`which gcloud`
BQ_BIN=`which bq`
JQ_BIN=`which jq`
SED_BIN=`which sed`

GCP_PROJECT_ID=${1}
REGION_ID_MULTI="US"
REGION_ID="us-central1"
CONN_ID="genai"
DATASET_ID="auto_comm_manager"
TABLE_ID="posts"
MODEL_ID="llm_model"
CFN_ID="batch_inference"

LOG_DATE=`date`
echo "###########################################################################################"
echo "${LOG_DATE} Creating BigQuery dataset ..."
"${BQ_BIN}" --location=${REGION_ID_MULTI} mk --dataset ${GCP_PROJECT_ID}:${DATASET_ID}

LOG_DATE=`date`
echo "###########################################################################################"
echo "${LOG_DATE} Creating BigQuery table ..."
"${BQ_BIN}" mk --table  ${GCP_PROJECT_ID}:${DATASET_ID}.${TABLE_ID} mail:STRING,comment:FLOAT,response:STRING


LOG_DATE=`date`
echo "###########################################################################################"
echo "${LOG_DATE} Creating BigQuery external connection ..."
"${BQ_BIN}" mk --connection --location=${REGION_ID_MULTI} --project_id=${GCP_PROJECT_ID} --connection_type=CLOUD_RESOURCE ${CONN_ID}
CONN_SA_ID=`bq --format=prettyjson show --connection ${GCP_PROJECT_ID}.${REGION_ID_MULTI}.${CONN_ID} | "${JQ_BIN}" -r .cloudResource.serviceAccountId`
echo "Conn ID is ${CONN_SA_ID}"

LOG_DATE=`date`
echo "###########################################################################################"
echo "${LOG_DATE} Creating BigQuery IAM policy bindings ..."
"${GCLOUD_BIN}" projects add-iam-policy-binding ${GCP_PROJECT_ID} --member=serviceAccount:${CONN_SA_ID} --role=roles/serviceusage.serviceUsageConsumer --condition=None
"${GCLOUD_BIN}" projects add-iam-policy-binding ${GCP_PROJECT_ID} --member=serviceAccount:${CONN_SA_ID} --role=roles/bigquery.connectionUser --condition=None

LOG_DATE=`date`
echo "###########################################################################################"
echo "${LOG_DATE} Creating BigQuery LLM  ..."
"${BQ_BIN}" query --nouse_legacy_sql "CREATE OR REPLACE MODEL \`${GCP_PROJECT_ID}.${DATASET_ID}.${MODEL_ID}\` REMOTE WITH CONNECTION \`${GCP_PROJECT_ID}.${REGION_ID_MULTI}.${CONN_ID}\` OPTIONS (REMOTE_SERVICE_TYPE = 'CLOUD_AI_LARGE_LANGUAGE_MODEL_V1');"

LOG_DATE=`date`
echo "###########################################################################################"
echo "${LOG_DATE} Deploying Cloud Function  ..."
"${GCLOUD_BIN}" functions deploy ${CFN_ID} --region=${REGION_ID} --runtime="python310" --source=cfn --entry-point="batch_inference" --trigger-http

LOG_DATE=`date`
echo "###########################################################################################"
echo "${LOG_DATE} Ammending streamlit file  ..."
"${SED_BIN}" -i s=___PROJECT_ID___=${GCP_PROJECT_ID}= frontend.py
"${SED_BIN}" -i s=___CFN_TRIGGER___=https://${REGION_ID}-${GCP_PROJECT_ID}.cloudfunctions.net/${CFN_ID}= frontend.py

LOG_DATE=`date`
echo "###########################################################################################"
echo "${LOG_DATE} Ammending build file  ..."
"${SED_BIN}" -i s=___PROJECT_ID___=${GCP_PROJECT_ID}= build_cloud_run_frontend.sh
"${SED_BIN}" -i s=___REGION_ID___=${REGION_ID}= build_cloud_run_frontend.sh

LOG_DATE=`date`
echo "###########################################################################################"
echo "${LOG_DATE} Setup complete!"




