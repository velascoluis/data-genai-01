# ............................................................
# STREAMLIT App
# ............................................................

import streamlit as st 
import datetime, time
import requests
import pandas_gbq
import pandas as pd
import json

from google.cloud import bigquery

PROJECT_ID = "___PROJECT_ID___"
DATASET_ID = "auto_comm_manager"
TABLE_ID = "posts"
CFN_TRIGGER = "___CFN_TRIGGER___"



def batch_inference():
    data = {"table_name": TABLE_ID, "dataset_name": DATASET_ID}
    requests.post(CFN_TRIGGER,  json=data)
    st.success("Batch successfully executed!")
  
def write_table_dataframe(comment,mail,response):
     
     df = pd.DataFrame(
    {
        "mail":mail,
        "comment": comment,
        "response" : response
    },index=[0])
     pandas_gbq.to_gbq(df, DATASET_ID+"."+TABLE_ID, project_id=PROJECT_ID,if_exists='append')

def read_table_dataframe(table_name):
    client = bigquery.Client()
    dataset_ref = bigquery.DatasetReference(PROJECT_ID, DATASET_ID)
    table_ref = dataset_ref.table(table_name)
    table = client.get_table(table_ref)
    df = client.list_rows(table).to_dataframe()
    return df

# .... App
st.markdown("![Google Cloud logo](https://www.gstatic.com/devrel-devsite/prod/v7f3d01938fc1f82b33d8c11166fff9e54cfd22895803f2cef46a3b200be855eb/cloud/images/cloud-logo.svg)")
st.markdown("## Data Cloud Deep Dives")
st.markdown("### Social media review app")
tab1, tab2, tab3 = st.tabs([" Write social media comment", " Community manager dashboard","Automated community manager"])

with tab1:
    st.markdown("#### Post: Google Pixel 7")
    st.markdown("![Pixel phone](https://med.csmobiles.com/562594-large_default/google-pixel-7-pro-5g-12gb-128gb-dual-sim-blanco.jpg)")
    st.write("The power and speed of Google Tensor G2.Google Tensor G2 is the same custom chip thats in our Pixel 7 Pro. Tensor G2 – along with 8 GB of RAM – makes Pixel 7a fast, secure, and efficient, with improved audio calls, great battery life, and amazing photos and videos.")
    with st.form("review_form"):
        comment = st.text_input("Enter your comment")
        mail = st.text_input("Your email")
        submitted = st.form_submit_button("Submit")
        response = ""
        if submitted:
            write_table_dataframe(comment,mail,response)
            st.success("Your comment has been recorded")

with tab2:
     with st.form("trad_form"):
         df = read_table_dataframe(TABLE_ID)
         mod_st = st.data_editor(df,use_container_width=True)
         reload = st.form_submit_button("Update table")
         if reload:
             write_table_dataframe(mod_st['comment'],mod_st['mail'],mod_st['response'])
             st.success("Table updated")
with tab3:
     with st.form("auto_form"):
         st.write("Automate responses")
         automate = st.form_submit_button("Launch LLM")
         if automate:
            batch_inference()
            df = read_table_dataframe(TABLE_ID+"_batch_inference")
            mod_st = st.data_editor(df,use_container_width=True)
            reload = st.form_submit_button("Update table")






