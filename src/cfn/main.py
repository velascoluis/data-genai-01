from google.cloud import bigquery


def batch_inference(request):
    request_json = request.get_json(silent=True)
    table_name = request_json['table_name']
    dataset_name = request_json['dataset_name']
    client = bigquery.Client()
    prompt_creation_query = f"""
      CREATE OR REPLACE TABLE {dataset_name}.{table_name}_prompts AS 
        (WITH prompts_full AS (
                  SELECT mail, comment,
                   concat('Generate a custom reply to this social media post message: ', comment, ',  customer email is: ', mail) as prompt
                    FROM {dataset_name}.{table_name}
                      )
        SELECT prompt from prompts_full
        )"""
    batch_inference_query = f"""
    CREATE OR REPLACE TABLE {dataset_name}.{table_name}_batch_inference AS 
    SELECT
        ml_generate_text_result['predictions'][0]['content'] AS generated_text,
        ml_generate_text_result['predictions'][0]['safetyAttributes'] AS safety_attributes,
    * EXCEPT (ml_generate_text_result)
FROM
    ML.GENERATE_TEXT(MODEL `{dataset_name}.llm_model`,
      (
          SELECT
            *
          FROM
          `{dataset_name}.{table_name}_prompts`
      ),
      STRUCT(
            0.2 AS temperature,
            120 AS max_output_tokens))"""

    client.query(prompt_creation_query)
    client.query(batch_inference_query)
    return {"response": "success"}
